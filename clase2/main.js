const sqlite3 = require('sqlite3').verbose();
const {app,BrowserWindow,dialog,ipcMain} = require("electron")
const alumnos = require("../db_sqlite")

alumnos.conectar("../datos.db",()=>{
    dialog.showErrorBox("Error de conexión",
                        "El archivo de la base de datos no existe o hay un error en la base de datos")
    app.exit();
})

ipcMain.on("save-alumno",(event,datos)=>{
    alumnos.save("../datos.db",datos,function(err){
        if (err){
            console.log(err)
            event.returnValue = 0
        }else
            event.returnValue = this.changes
    })
})

ipcMain.on("getAll",(event,campoOrden="nombre",
                    tipoOrden="asc",where="",limit=0,offset=0)=>{
    alumnos.getAll("../datos.db",(err,rows)=>{
        //console.log(rows)
        event.returnValue = rows
    },campoOrden,tipoOrden,where,limit,offset)
})

ipcMain.on("count",(event,where)=>{
    alumnos.count("../datos.db",where,(err,rows)=>{
        if (err){
            console.log("error al determinar la cantidad de registros: "+err)
            event.returnValue = -1
        }
        else{
            console.log(rows[0].cantidad)
            event.returnValue = rows[0].cantidad
        }
    })
})

app.on("ready",()=>{
    let ventana = new BrowserWindow({
        visible:false,
        webPreferences: {
            preload: __dirname + "\\preload.js",
            nodeIntegration :false,
            contextIsolation: true
        }
    });
    ventana.setMenu(null)
    ventana.loadFile("alumnos.html")
    ventana.maximize()
    ventana.webContents.openDevTools()
    ventana.show()
})

/*
var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'jose',
  password : '12345678',
  database : 'datos'
});
 
connection.connect((err)=>{
    if (err)
        console.log("No se pudo abrir la base de datos mysql: "+err)
    else{
        console.log("conexion a mysql exitosa")
        connection.query("select * from alumno",function(err,filas){
            if (err)
                console.log("error en la consulta: "+err)
            else{
                console.log("query a mysql ok")
                console.log(filas)
            }
            connection.end()
        })
    }
    
});
*/
