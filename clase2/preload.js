const {ipcRenderer,contextBridge} = require("electron")

const REGISTROS_PAGINA = 20
let paginaActual = 1
let previous,next
let nroPaginas = 1
campoOrden = "nombre"
tipoOrden = "desc"


contextBridge.exposeInMainWorld("api",{
    "saveAlumno" : saveAlumno,
    "recargarAlumnos": cargarAlumnos
})

function saveAlumno(datos){
    result = ipcRenderer.sendSync("save-alumno",datos)
    return result
}

window.onload = function(){
    document.getElementById("id").onclick = cambiarOrden
    document.getElementById("nombre").onclick = cambiarOrden
    document.getElementById("poblacion").onclick = cambiarOrden
    document.getElementById("buscarPor").onkeyup = filtrar
    document.getElementById("next-page").onclick = avanzarPagina

    previous = document.getElementById("previous-page")
    next = document.getElementById("next-page")

    previous.onclick = retrocederPagina
    
    btnRecargar.onclick = cargarAlumnos
    cargarAlumnos()
    mostrarMarcadoresPaginas()
}

function filtrar(){
    paginaActual = 1
    cargarAlumnos()
    mostrarMarcadoresPaginas()
}

function retrocederPagina(){
    if (paginaActual > 0){
        paginaActual--
        cargarAlumnos()
    }
    console.log(paginaActual)
    next.className = "page-link"
    if (paginaActual == 1)
        previous.className = "d-none page-link"
}

function avanzarPagina(){
    if (paginaActual < nroPaginas){
        paginaActual++
        cargarAlumnos()
        previous.className = "page-link"
    }
    if (paginaActual == nroPaginas)
        next.className = "page-link d-none"
}

function cambiarOrden(){
    console.log(this.id)
    
    if (campoOrden == this.id)
        if (tipoOrden == "asc")
            tipoOrden = "desc"
        else
            tipoOrden = "asc"
    else
        campoOrden = this.id

    cargarAlumnos()
}

function eliminarMarcadoresPaginas(){
    lista = document.getElementById("lista-paginacion")
    n = lista.childNodes.length
    for (i=3;i<=n-3;i++)
        lista.removeChild(lista.childNodes[3])
}

function mostrarMarcadoresPaginas(){
    eliminarMarcadoresPaginas()
    nroPaginas = Math.ceil(cantidad / REGISTROS_PAGINA)
    lista = document.getElementById("lista-paginacion")
    for (i=1;i<=nroPaginas;i++){
        
        a = document.createElement("a")
        a.className = "page-link"
        a.href = "#"
        a.innerText = i
        a.onclick = cambiarPagina

        li = document.createElement("li")
        li.className = "page-item page"
        li.appendChild(a)

        lista.insertBefore(li,document.getElementById("li-next-page"))
    }
}

function cambiarPagina(){
    paginaActual = this.innerText

    if (paginaActual == nroPaginas){
        next.className = "page-link d-none"
        previous.className = "page-link"
    }
    else
        if (paginaActual == 1){
            previous.className = "page-link d-none"
            next.className = "page-link"
        }else{
            previous.className = "page-link"
            next.className = "page-link"
        }
    cargarAlumnos()
}

function cargarAlumnos(){
    input = document.getElementById("buscarPor")
    where = `${campo.value} like "%${input.value}%"`

    cantidad = ipcRenderer.sendSync("count",where)
    console.log(cantidad)
    
    if (cantidad > REGISTROS_PAGINA){
        document.getElementById("paginas").className = ""
        document.getElementById("info-paginas").className = ""
    }
    else{
        document.getElementById("info-paginas").className = "d-none"
        document.getElementById("paginas").className = "d-none"
    }
         
    document.getElementById("nro-registros").innerText = cantidad
    document.getElementById("registros-pagina").innerText = REGISTROS_PAGINA
    
    offset = (paginaActual - 1) * REGISTROS_PAGINA

    alumnos = ipcRenderer.sendSync("getAll",campoOrden,tipoOrden,where,REGISTROS_PAGINA,offset)
    console.log(alumnos)
    llenarTablaAlumnos(alumnos)
}

function eliminarFilas(tabla){
    while (tabla.rows.length>1)
        tabla.deleteRow(1)
}

function llenarTablaAlumnos(alumnos){
    let tablaCLientes = document.getElementById("tablaCLientes")
    eliminarFilas(tablaCLientes)

    alumnos.forEach((c)=>{
        fila = tablaCLientes.tBodies[0].insertRow(-1)
        celda = fila.insertCell(-1)
        celda.innerText = c.id
        celda = fila.insertCell(-1)
        celda.innerText = c.nombre
        celda = fila.insertCell(-1)
        celda.innerText = c.poblacion
        celda = fila.insertCell(-1)
    })
 }