
const myModal = new bootstrap.Modal('#registrar', {
    keyboard: false
})  

formAlumno = document.forms[0]
btnNuevo.onclick = nuevoAlumno
btnGuardar.onclick = guardar

function nuevoAlumno(){
    
    myModal.show()
}

function guardar(){
    formAlumno.classList.add('was-validated')
    if (formAlumno.checkValidity()){
        datos = {
            "cedula":formAlumno.cedula.value,
            "nombre": formAlumno.nombre.value,
            "correo": formAlumno.correo.value
        }

        if (api.saveAlumno(datos)){
            myModal.hide()
            formAlumno.classList.remove('was-validated')
            formAlumno.reset()
            api.recargarAlumnos()
        }else{
            console.log("error al registrar")
        }
    }
}