
const REGISTROS_PAGINA = 10
let paginaActual = 1
let previous,next
let nroPaginas = 1
let campoOrden = "nombre"
let tipoOrden = "desc"
let myModal 
let tablaAlumnos

window.onload = function(){

    formAlumno = document.forms[0]
    tablaAlumnos = document.getElementById("tablaAlumnos")
    previous = document.getElementById("previous-page")
    next = document.getElementById("next-page")

    btnNuevo.onclick = nuevoAlumno
    btnEliminarMarcados.onclick = eliminarAlumnosMarcados
    btnGuardar.onclick = guardar
    btnRecargar.onclick = recargar
    chkMarcarTodos.onclick = marcarTodos

    document.getElementById("id").onclick = cambiarOrden
    document.getElementById("nombre").onclick = cambiarOrden
    document.getElementById("correo").onclick = cambiarOrden
    document.getElementById("buscarPor").onkeyup = filtrar
    document.getElementById("next-page").onclick = avanzarPagina
    previous.onclick = retrocederPagina
    
	let ciudades = api.getCiudades()
	console.log(ciudades)
    if (ciudades != null)
	    llenarCiudades(ciudades)
	
    cargarAlumnos()
    mostrarMarcadoresPaginas()

    myModal = new bootstrap.Modal('#registrar', {
        keyboard: false
    })  
}

function llenarCiudades(ciudades){
  let select = document.getElementById("idCiudad")

  for (i=0;i<ciudades.length;i++){
    let option = document.createElement("option")
    
    option.value = ciudades[i].id
    option.innerText = ciudades[i].nombre

    select.appendChild(option)
  }
}

function marcarTodos(){
    marcados = []

    checkboxes = document.getElementsByClassName("marcar")
    for (i=0;i<checkboxes.length;i++){
        checkboxes[i].checked = this.checked
        if (this.checked)
            marcados.push(checkboxes[i].value)
    }
    console.log(marcados)
}

function recargar(){
    mostrarMarcadoresPaginas()
    cargarAlumnos()
}

function nuevoAlumno(){
    formAlumno.idAlumno.value = 0
    formAlumno.reset()
    myModal.show()
}

function guardar(){
    formAlumno.classList.add('was-validated')
    if (formAlumno.checkValidity()){
        
        saveAlumno(formAlumno)
    }
}

function updateAlumno(datos){
    if (api.updateAlumno(datos)){
        Swal.fire({
            text:"Actualizado exitosamente!",
            toast:true,
            position:'top-end',
            showConfirmButton: false,
            timer: 10000,
            timerProgressBar: true,
            icon: "success"
        });
        myModal.hide()
        formAlumno.classList.remove('was-validated')
        formAlumno.reset()
        
    }else{
        console.log("error al registrar")
        Swal.fire({
            text:"Error al intentar actualizar el alumno",
            toast:true,
            position:'top-end',
            showConfirmButton: false,
            timer: 10000,
            timerProgressBar: true,
            icon: "error"
        });
    }
}

function insertAlumno(datos){
    if (api.insertAlumno(datos)){
        Swal.fire({
            text:"Registrado exitosamente!",
            toast:true,
            position:'top-end',
            showConfirmButton: false,
            timer: 10000,
            timerProgressBar: true,
            icon: "success"
        });
        myModal.hide()
        formAlumno.classList.remove('was-validated')
        formAlumno.reset()
        cargarAlumnos()
    }else{
        console.log("error al registrar")
        Swal.fire({
            text:"Error al intentar registrar el alumno",
            toast:true,
            position:'top-end',
            showConfirmButton: false,
            timer: 10000,
            timerProgressBar: true,
            icon: "error"
        });
    }
}

function cargarAlumnos(){
    input = document.getElementById("buscarPor")
    campoWhere = campo.value
    valorWhere = "%"+input.value+"%"

    cantidad = api.getCountAlumnos(campoWhere,valorWhere)
    console.log(cantidad)
    
    if (cantidad > REGISTROS_PAGINA){
        document.getElementById("paginas").className = ""
        document.getElementById("info-paginas").className = ""
    }
    else{
        document.getElementById("info-paginas").className = "d-none"
        document.getElementById("paginas").className = "d-none"
    }
         
    document.getElementById("nro-registros").innerText = cantidad
    document.getElementById("registros-pagina").innerText = REGISTROS_PAGINA
    
    offset = (paginaActual - 1) * REGISTROS_PAGINA

    alumnos = api.getAllAlumnos(campoOrden,tipoOrden,campoWhere,
                                valorWhere,REGISTROS_PAGINA,offset)
    console.log(alumnos)
    llenarTablaAlumnos(alumnos)
}

function filtrar(){
    paginaActual = 1
    cargarAlumnos()
    mostrarMarcadoresPaginas()
}

function retrocederPagina(){
    if (paginaActual > 0){
        paginaActual--
        cargarAlumnos()
    }
    console.log(paginaActual)
    next.className = "page-link"
    if (paginaActual == 1)
        previous.className = "d-none page-link"
}

function avanzarPagina(){
    if (paginaActual < nroPaginas){
        paginaActual++
        cargarAlumnos()
        previous.className = "page-link"
    }
    if (paginaActual == nroPaginas)
        next.className = "page-link d-none"
}

function cambiarOrden(){
    console.log(this.id)
    
    if (campoOrden == this.id)
        if (tipoOrden == "asc")
            tipoOrden = "desc"
        else
            tipoOrden = "asc"
    else
        campoOrden = this.id

    cargarAlumnos()
}

function eliminarMarcadoresPaginas(){
    lista = document.getElementById("lista-paginacion")
    n = lista.childNodes.length
    for (i=3;i<=n-3;i++)
        lista.removeChild(lista.childNodes[3])
}

function mostrarMarcadoresPaginas(){
    eliminarMarcadoresPaginas()
    nroPaginas = Math.ceil(cantidad / REGISTROS_PAGINA)
    lista = document.getElementById("lista-paginacion")
    for (i=1;i<=nroPaginas;i++){
        
        a = document.createElement("a")
        a.className = "page-link"
        a.href = "#"
        a.innerText = i
        a.onclick = cambiarPagina

        li = document.createElement("li")
        li.className = "page-item page"
        li.appendChild(a)

        lista.insertBefore(li,document.getElementById("li-next-page"))
    }
}

function cambiarPagina(){
    paginaActual = this.innerText

    if (paginaActual == nroPaginas){
        next.className = "page-link d-none"
        previous.className = "page-link"
    }
    else
        if (paginaActual == 1){
            previous.className = "page-link d-none"
            next.className = "page-link"
        }else{
            previous.className = "page-link"
            next.className = "page-link"
        }
    cargarAlumnos()
}

function eliminarFilas(tabla){
    while (tabla.rows.length>1)
        tabla.deleteRow(1)
}

function llenarTablaAlumnos(alumnos){
    
    eliminarFilas(tablaAlumnos)

    alumnos.forEach((c)=>{
        fila = tablaAlumnos.tBodies[0].insertRow(-1)
        fila.onmouseover = seleccionarFila
        fila.onmouseout = deseleccionaFila

        celda = fila.insertCell(-1)

        check = document.createElement("input")
        check.type = "checkbox"
        check.value = c.id
        check.onclick = marcarAlumno
        check.className = "marcar"

        celda.appendChild(check)

        celda = fila.insertCell(-1)
        
        a = document.createElement("a")
        a.href="#"
        a.onclick = verAlumno
        a.idAlumno = c.id
        a.innerText = c.nombre
        
        celda.appendChild(a)

        celda = fila.insertCell(-1)
        celda.innerText = c.correo
		
		celda = fila.insertCell(-1)
		celda.innerText = c.ciudad.nombre
        
        celda = fila.insertCell(-1)
        boton = document.createElement("button")
        boton.className = "btn btn-danger"
        boton.innerText = "Eliminar"
        boton.idAlumno = c.id
        boton.onclick = eliminarAlumno
        celda.appendChild(boton)
    })
 }

 function verAlumno(){
    console.log(this.idAlumno)
    alumno = api.getAlumno(this.idAlumno)
    if (alumno){
        formAlumno.idAlumno.value = this.idAlumno
        formAlumno.cedula.value = alumno.cedula
        formAlumno.nombre.value = alumno.nombre
        formAlumno.correo.value = alumno.correo
		formAlumno.idCiudad.value = alumno.ciudad.id
		
        myModal.show()
    }else
        console.log("el registro fue eliminado")
 }

 let marcados = []

 function marcarAlumno(){
    if (this.checked){
        marcados.push(this.value)
        
    }else{
        posicion = marcados.indexOf(this.value)
        marcados.splice(posicion,1)
    }
    console.log(marcados)
 }

function deseleccionaFila(){
    //console.log("deseleccionar "+this.rowIndex)
}

function seleccionarFila(){
    //console.log("seleccionar "+this.rowIndex)
}

function eliminarAlumnosMarcados(){
    if (marcados.length > 0){
        Swal.fire({
            title: `¿Está seguro que desea eliminar ${marcados.length} alumnos?`,
            text: "No puede deshacer esta acción",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Si",
            cancelButtonText: "No"
          }).then((result) => {
            if (result.isConfirmed) {
                console.log("eliminar "+marcados)
                if (api.removeAlumno(marcados)){
                    
                    console.log("eliminado")
                    cargarAlumnos()
                    mostrarMarcadoresPaginas()             
    
                    Swal.fire({
                        text:`Elimino ${marcados.length} alumnos`,
                        toast:true,
                        position:'top-end',
                        showConfirmButton: false,
                        timer: 10000,
                        timerProgressBar: true,
                        icon: "success"
                    });
                }
            }
          });
    }
}

 function eliminarAlumno(){
    
    Swal.fire({
        title: "¿Está seguro?",
        text: "No puede deshacer esta acción",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Si",
        cancelButtonText: "No"
      }).then((result) => {
        if (result.isConfirmed) {
            console.log("eliminar "+this.idAlumno)
            if (api.removeAlumno(this.idAlumno)){
                
                console.log("eliminado")
                fila = this.parentElement.parentElement.rowIndex
                tablaAlumnos.deleteRow(fila)
                
                if (tablaAlumnos.rows.length == 1){
                    if (paginaActual > 1)
                        paginaActual--;

                    cargarAlumnos()
                    mostrarMarcadoresPaginas()
                }               

                Swal.fire({
                    text:"Elimino el alumno!",
                    toast:true,
                    position:'top-end',
                    showConfirmButton: false,
                    timer: 10000,
                    timerProgressBar: true,
                    icon: "success"
                });
            }
        }
      });
    
 }