const sqlite3 = require('sqlite3')
const TABLA = "alumno"

function conectar(nombrBd:string,callbackError:any=undefined):any{
    let db = new sqlite3.Database(nombrBd, sqlite3.OPEN_READWRITE, (err:any) => {
        if (err) {
          console.error("error: "+err.message);
          if (callbackError)
            callbackError()
        }
        console.log('Connected to SQLite the database.');
    });
    return db;
}

function count(nombreBd:string, where:string, callback:any):any{
    let sql = `SELECT count(*) as cantidad FROM ${TABLA} `;
    
    if (where != "")
        sql += ` where ${where} `

    let conn = conectar(nombreBd);
    if (!conn)        
        return null;
    else {
        conn.all(sql, [], callback);
        conn.close()
    } 
}

function get(nombreBd:string,id:number,callback:any):any{
    let sql = `SELECT * FROM ${TABLA} where id = ?`;

    console.log(sql)
    let conn = conectar(nombreBd);
    if (!conn)        
        return null;
    else {
        conn.get(sql, [id], callback);
        conn.close()
    } 
}

function getAll(nombreBd:string,callback:any,
                campoOrden="nombre",
                tipoOrden="asc",
                where="",limit=0,offset=0):any{

    let sql = `SELECT * FROM ${TABLA} `;
    
    if (where != "")
        sql += ` where ${where} `

    sql += ` order by ${campoOrden} ${tipoOrden}`;

    if (limit != 0)
        sql += ` limit ${limit} offset ${offset}`

    console.log(sql)
    let conn = conectar(nombreBd);
    if (!conn)        
        return null;
    else {
        conn.all(sql, [], callback);
        conn.close()
    } 
}

function insert(nombreBd:string,datos:any,callback:any):any{
    let sql = "insert into alumno (cedula,nombre,correo) values (?,?,?)";
    let conn = conectar(nombreBd);

    if (!conn)        
        return null;
    else {
        conn.run(sql, [datos.cedula,datos.nombre,datos.correo], callback);
        conn.close()
    } 
}

function update(nombreBd:string,datos:any,callback:any):any{
    let sql = "update alumno set cedula=?,nombre=?,correo=? where id=?";
    let conn = conectar(nombreBd);

    if (!conn)        
        return null;
    else {
        conn.run(sql, [datos.cedula,datos.nombre,datos.correo,datos.id], callback);
        conn.close()
    } 
}

function remove(nombreBd:string,id:number,callback:any):any{
    let sql;

    if (Array.isArray(id))
        sql = `delete from alumno where id in (${id})`;
    else
        sql = `delete from alumno where id=${id}`;

    let conn = conectar(nombreBd);

    if (!conn)        
        return null;
    else {
        conn.run(sql, [], callback);
        conn.close()
    } 
}

module.exports = { conectar, getAll, count, insert, remove, get, update }