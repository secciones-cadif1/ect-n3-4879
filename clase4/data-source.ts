import { DataSource } from "typeorm"
import {Alumno} from "./entidades/Alumno"
import {Ciudad} from "./entidades/Ciudad"

export const appDataSource = new DataSource({
    type: "sqlite",
    database: "datos.db",
    logging: true,
    entities: [Alumno,Ciudad],
})