"use strict";
var __esDecorate = (this && this.__esDecorate) || function (ctor, descriptorIn, decorators, contextIn, initializers, extraInitializers) {
    function accept(f) { if (f !== void 0 && typeof f !== "function") throw new TypeError("Function expected"); return f; }
    var kind = contextIn.kind, key = kind === "getter" ? "get" : kind === "setter" ? "set" : "value";
    var target = !descriptorIn && ctor ? contextIn["static"] ? ctor : ctor.prototype : null;
    var descriptor = descriptorIn || (target ? Object.getOwnPropertyDescriptor(target, contextIn.name) : {});
    var _, done = false;
    for (var i = decorators.length - 1; i >= 0; i--) {
        var context = {};
        for (var p in contextIn) context[p] = p === "access" ? {} : contextIn[p];
        for (var p in contextIn.access) context.access[p] = contextIn.access[p];
        context.addInitializer = function (f) { if (done) throw new TypeError("Cannot add initializers after decoration has completed"); extraInitializers.push(accept(f || null)); };
        var result = (0, decorators[i])(kind === "accessor" ? { get: descriptor.get, set: descriptor.set } : descriptor[key], context);
        if (kind === "accessor") {
            if (result === void 0) continue;
            if (result === null || typeof result !== "object") throw new TypeError("Object expected");
            if (_ = accept(result.get)) descriptor.get = _;
            if (_ = accept(result.set)) descriptor.set = _;
            if (_ = accept(result.init)) initializers.unshift(_);
        }
        else if (_ = accept(result)) {
            if (kind === "field") initializers.unshift(_);
            else descriptor[key] = _;
        }
    }
    if (target) Object.defineProperty(target, contextIn.name, descriptor);
    done = true;
};
var __runInitializers = (this && this.__runInitializers) || function (thisArg, initializers, value) {
    var useValue = arguments.length > 2;
    for (var i = 0; i < initializers.length; i++) {
        value = useValue ? initializers[i].call(thisArg, value) : initializers[i].call(thisArg);
    }
    return useValue ? value : void 0;
};
var __setFunctionName = (this && this.__setFunctionName) || function (f, name, prefix) {
    if (typeof name === "symbol") name = name.description ? "[".concat(name.description, "]") : "";
    return Object.defineProperty(f, "name", { configurable: true, value: prefix ? "".concat(prefix, " ", name) : name });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Ciudad = void 0;
var typeorm_1 = require("typeorm");
var Alumno_1 = require("./Alumno");
var Ciudad = function () {
    var _classDecorators = [(0, typeorm_1.Entity)()];
    var _classDescriptor;
    var _classExtraInitializers = [];
    var _classThis;
    var _id_decorators;
    var _id_initializers = [];
    var _id_extraInitializers = [];
    var _nombre_decorators;
    var _nombre_initializers = [];
    var _nombre_extraInitializers = [];
    var _capital_decorators;
    var _capital_initializers = [];
    var _capital_extraInitializers = [];
    var _poblacion_decorators;
    var _poblacion_initializers = [];
    var _poblacion_extraInitializers = [];
    var _id_estado_decorators;
    var _id_estado_initializers = [];
    var _id_estado_extraInitializers = [];
    var _alumnos_decorators;
    var _alumnos_initializers = [];
    var _alumnos_extraInitializers = [];
    var Ciudad = _classThis = /** @class */ (function () {
        function Ciudad_1() {
            this.id = __runInitializers(this, _id_initializers, void 0);
            this.nombre = (__runInitializers(this, _id_extraInitializers), __runInitializers(this, _nombre_initializers, void 0));
            this.capital = (__runInitializers(this, _nombre_extraInitializers), __runInitializers(this, _capital_initializers, void 0));
            this.poblacion = (__runInitializers(this, _capital_extraInitializers), __runInitializers(this, _poblacion_initializers, void 0));
            this.id_estado = (__runInitializers(this, _poblacion_extraInitializers), __runInitializers(this, _id_estado_initializers, void 0));
            this.alumnos = (__runInitializers(this, _id_estado_extraInitializers), __runInitializers(this, _alumnos_initializers, void 0));
            __runInitializers(this, _alumnos_extraInitializers);
        }
        return Ciudad_1;
    }());
    __setFunctionName(_classThis, "Ciudad");
    (function () {
        var _metadata = typeof Symbol === "function" && Symbol.metadata ? Object.create(null) : void 0;
        _id_decorators = [(0, typeorm_1.PrimaryGeneratedColumn)()];
        _nombre_decorators = [(0, typeorm_1.Column)()];
        _capital_decorators = [(0, typeorm_1.Column)()];
        _poblacion_decorators = [(0, typeorm_1.Column)()];
        _id_estado_decorators = [(0, typeorm_1.Column)()];
        _alumnos_decorators = [(0, typeorm_1.OneToMany)(function () { return Alumno_1.Alumno; }, function (alumno) { return alumno.ciudad; }, {})];
        __esDecorate(null, null, _id_decorators, { kind: "field", name: "id", static: false, private: false, access: { has: function (obj) { return "id" in obj; }, get: function (obj) { return obj.id; }, set: function (obj, value) { obj.id = value; } }, metadata: _metadata }, _id_initializers, _id_extraInitializers);
        __esDecorate(null, null, _nombre_decorators, { kind: "field", name: "nombre", static: false, private: false, access: { has: function (obj) { return "nombre" in obj; }, get: function (obj) { return obj.nombre; }, set: function (obj, value) { obj.nombre = value; } }, metadata: _metadata }, _nombre_initializers, _nombre_extraInitializers);
        __esDecorate(null, null, _capital_decorators, { kind: "field", name: "capital", static: false, private: false, access: { has: function (obj) { return "capital" in obj; }, get: function (obj) { return obj.capital; }, set: function (obj, value) { obj.capital = value; } }, metadata: _metadata }, _capital_initializers, _capital_extraInitializers);
        __esDecorate(null, null, _poblacion_decorators, { kind: "field", name: "poblacion", static: false, private: false, access: { has: function (obj) { return "poblacion" in obj; }, get: function (obj) { return obj.poblacion; }, set: function (obj, value) { obj.poblacion = value; } }, metadata: _metadata }, _poblacion_initializers, _poblacion_extraInitializers);
        __esDecorate(null, null, _id_estado_decorators, { kind: "field", name: "id_estado", static: false, private: false, access: { has: function (obj) { return "id_estado" in obj; }, get: function (obj) { return obj.id_estado; }, set: function (obj, value) { obj.id_estado = value; } }, metadata: _metadata }, _id_estado_initializers, _id_estado_extraInitializers);
        __esDecorate(null, null, _alumnos_decorators, { kind: "field", name: "alumnos", static: false, private: false, access: { has: function (obj) { return "alumnos" in obj; }, get: function (obj) { return obj.alumnos; }, set: function (obj, value) { obj.alumnos = value; } }, metadata: _metadata }, _alumnos_initializers, _alumnos_extraInitializers);
        __esDecorate(null, _classDescriptor = { value: _classThis }, _classDecorators, { kind: "class", name: _classThis.name, metadata: _metadata }, null, _classExtraInitializers);
        Ciudad = _classThis = _classDescriptor.value;
        if (_metadata) Object.defineProperty(_classThis, Symbol.metadata, { enumerable: true, configurable: true, writable: true, value: _metadata });
        __runInitializers(_classThis, _classExtraInitializers);
    })();
    return Ciudad = _classThis;
}();
exports.Ciudad = Ciudad;
