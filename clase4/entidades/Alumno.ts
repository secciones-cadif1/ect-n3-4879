import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from "typeorm"
import {Ciudad} from "./Ciudad"

@Entity()
export class Alumno {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    nombre: string

    @Column()
    cedula: string

    @Column()
    correo: string
	
	@ManyToOne(()=>Ciudad,{
        eager:true
    })
    @JoinColumn([
        { name: "id_ciudad", referencedColumnName: "id" }
    ])
    ciudad: Ciudad
}