import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm"
import {Alumno} from "./Alumno"

@Entity()
export class Ciudad {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    nombre: string

    @Column()
    capital: number

    @Column()
    poblacion: number

    @Column()
    id_estado: number
	
	@OneToMany(() => Alumno, (alumno: Alumno) => alumno.ciudad,{
    
    })
    alumnos: Alumno[]
}