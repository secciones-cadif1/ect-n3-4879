const {app,BrowserWindow,dialog,ipcMain} = require("electron")
const {appDataSource} = require("./data-source")
const {Alumno} = require("./entidades/Alumno")
const {Ciudad} = require("./entidades/Ciudad")
const {Like} = require("typeorm")

appDataSource.initialize()
    .then(() => {
        console.log("datasource inicializado exitosamente")
    })
    .catch((error:any) => console.log(error))

ipcMain.on("guardar-alumno",(event:any,datos:any)=>{
    // para nuevos registros y para registros existentes
    // create y update. Si el objeto existe, lo actualiza
    // si no existe, lo inserta en la tabla
    appDataSource.getRepository(Alumno).save(datos)
    .then(()=>{
        event.returnValue = 1
    })
    .catch(()=>{
        event.returnValue = -1
    })
})

ipcMain.on("getAll-alumno",(event:any,campoOrden="nombre",
                            tipoOrden="asc",
                            campoWhere="",valorWhere="",
                            limit=0,offset=0)=>{

    let opciones = {
        take: limit,
        skip :offset,
        order : {
            [campoOrden] : tipoOrden
        },
        where : {
            [campoWhere] : Like(valorWhere)
        }
    }

    appDataSource.getRepository(Alumno).find(opciones)
    .then((registros:any)=>{
        event.returnValue = registros
    })
    .catch((error:any)=>{
        console.log("error: "+error)
        event.returnValue = []
    })
})

ipcMain.on("get-ciudades",(event:any)=>{
    appDataSource.getRepository(Ciudad).find({
		order: {
            nombre: "ASC"
        }
	})
    .then((ciudades:any)=>{
        event.returnValue = ciudades
    })
    .catch((error:any)=>{
        console.log("error: "+error)
        event.returnValue = null
    })
})

ipcMain.on("get-ciudad",(event:any,idValue:number)=>{
    
    appDataSource.getRepository(Ciudad).findOneBy({
        id: idValue,
    })
    .then((ciudad:any)=>{
        console.log(ciudad)
        event.returnValue = ciudad
    })
    .catch((error:any)=>{
            console.log(error)
            event.returnValue = -1
    })
})

ipcMain.on("get-alumno",(event:any,idValor:number)=>{
    appDataSource.getRepository(Alumno).findOneBy({
        id :idValor
    })
    .then((alumno:any)=>{
        console.log(alumno)
        event.returnValue = alumno
    })
    .catch((error:any)=>{
        console.log("error: "+error)
        event.returnValue = null
    })
})

ipcMain.on("remove-alumno",(event:any,id:number)=>{
    appDataSource.getRepository(Alumno).delete(id)
    .then(()=>{
        event.returnValue = 1
    })
    .catch(()=>{
        event.returnValue = -1
    })
})



ipcMain.on("count-alumno",(event:any,campoWhere:string,valorWhere:string)=>{
    appDataSource.getRepository(Alumno)
    .createQueryBuilder("cliente")
    .where(`${campoWhere} like '${valorWhere}'`)
    .getCount()
    .then((valor:number)=>{
        event.returnValue = valor
    })
    .catch((error:any)=>{
        console.log(error)
        event.returnValue = -1
    })
})
console.log(__dirname)

app.on("ready",()=>{
    let ventana = new BrowserWindow({
        visible:false,
        webPreferences: {
            preload: __dirname + "\\preload.js",
            nodeIntegration :false,
            contextIsolation: true
        }
    });
  //  ventana.setMenu(null)
    ventana.loadFile("../clase4/alumnos.html")
    ventana.maximize()
    ventana.webContents.openDevTools()
    ventana.show()

})

