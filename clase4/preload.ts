const {ipcRenderer,contextBridge} = require("electron")
//import {Alumno} from "./entidades/Alumno"

contextBridge.exposeInMainWorld("api",{
    "saveAlumno" : saveAlumno,
    "getAlumno": getAlumno,
    "getCountAlumnos" : getCountAlumnos,
    "getAllAlumnos": getAllAlumnos,
    "removeAlumno": removeAlumno,
	"getCiudades": getCiudades
})

function getCiudad(id:number){
    let ciudad = ipcRenderer.sendSync("get-ciudad",id)
    return ciudad
}

function getCiudades():any{
    let ciudades = ipcRenderer.sendSync('get-ciudades');
    return ciudades
}

function getAlumno(id:number):any{
    let result = ipcRenderer.sendSync("get-alumno",id)
    return result
}

function removeAlumno(datos:any):any{
    let result = ipcRenderer.sendSync("remove-alumno",datos)
    return result
}

function saveAlumno(form:any):any{
    let alumno = new Alumno()

    if (form.idAlumno.value!=0)
        alumno.id = parseInt(form.idAlumno.value)

    alumno.cedula = form.cedula.value
    alumno.correo = form.correo.value
    alumno.nombre = form.nombre.value
	alumno.ciudad = getCiudad(form.idCiudad.value)

    let result = ipcRenderer.sendSync("guardar-alumno",alumno)
    return result
}

function getCountAlumnos(campoWhere:string,valorWhere:string):any{
    return ipcRenderer.sendSync("count-alumno",campoWhere,valorWhere)
}

function getAllAlumnos(campoOrden:string,tipoOrden:string,
                        campoWhere:string,valorWhere:string,
                        cantidad:number,offset:number):any{
    return ipcRenderer.sendSync("getAll-alumno",campoOrden,tipoOrden,
                                campoWhere,valorWhere,cantidad,offset)
}
