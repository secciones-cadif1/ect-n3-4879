const sqlite3 = require('sqlite3')
const TABLA = "alumno"

function conectar(nombrBd,callbackError){
    let db = new sqlite3.Database(nombrBd, sqlite3.OPEN_READWRITE, (err) => {
        if (err) {
          console.error("error: "+err.message);
          if (callbackError)
            callbackError()
        }
        console.log('Connected to SQLite the database.');
    });
    return db;
}

function count(nombreBd, where, callback){
    let sql = `SELECT count(*) as cantidad FROM ${TABLA} `;
    
    if (where != "")
        sql += ` where ${where} `

    let conn = conectar(nombreBd);
    if (!conn)        
        return null;
    else {
        conn.all(sql, [], callback);
        conn.close()
    } 
}

function get(nombreBd,id,callback){
    let sql = `SELECT * FROM ${TABLA} where id = ?`;

    console.log(sql)
    let conn = conectar(nombreBd);
    if (!conn)        
        return null;
    else {
        conn.get(sql, [id], callback);
        conn.close()
    } 
}

function getAll(nombreBd,callback,
                campoOrden="nombre",
                tipoOrden="asc",
                where="",limit=0,offset=0){

    let sql = `SELECT * FROM ${TABLA} `;
    
    if (where != "")
        sql += ` where ${where} `

    sql += ` order by ${campoOrden} ${tipoOrden}`;

    if (limit != 0)
        sql += ` limit ${limit} offset ${offset}`

    console.log(sql)
    let conn = conectar(nombreBd);
    if (!conn)        
        return null;
    else {
        conn.all(sql, [], callback);
        conn.close()
    } 
}

function insert(nombreBd,datos,callback){
    sql = "insert into alumno (cedula,nombre,correo) values (?,?,?)";
    let conn = conectar(nombreBd);

    if (!conn)        
        return null;
    else {
        conn.run(sql, [datos.cedula,datos.nombre,datos.correo], callback);
        conn.close()
    } 
}

function update(nombreBd,datos,callback){
    sql = "update alumno set cedula=?,nombre=?,correo=? where id=?";
    let conn = conectar(nombreBd);

    if (!conn)        
        return null;
    else {
        conn.run(sql, [datos.cedula,datos.nombre,datos.correo,datos.id], callback);
        conn.close()
    } 
}

function remove(nombreBd,id,callback){
    if (Array.isArray(id))
        sql = `delete from alumno where id in (${id})`;
    else
        sql = `delete from alumno where id=${id}`;

    let conn = conectar(nombreBd);

    if (!conn)        
        return null;
    else {
        conn.run(sql, [], callback);
        conn.close()
    } 
}

module.exports = { conectar, getAll, count, insert, remove, get, update }