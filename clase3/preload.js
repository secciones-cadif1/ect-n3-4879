const {ipcRenderer,contextBridge} = require("electron")

contextBridge.exposeInMainWorld("api",{
    "insertAlumno" : insertAlumno,
    "getAlumno": getAlumno,
    "getCountAlumnos" : getCountAlumnos,
    "getAllAlumnos": getAllAlumnos,
    "updateAlumno" : updateAlumno,
    "removeAlumno": removeAlumno,
})

function getAlumno(id){
    result = ipcRenderer.sendSync("get-alumno",id)
    return result
}

function removeAlumno(datos){
    result = ipcRenderer.sendSync("remove-alumno",datos)
    return result
}

function insertAlumno(datos){
    result = ipcRenderer.sendSync("insert-alumno",datos)
    return result
}

function updateAlumno(datos){
    result = ipcRenderer.sendSync("update-alumno",datos)
    return result
}

function getCountAlumnos(where){
    return ipcRenderer.sendSync("count-alumno",where)
}

function getAllAlumnos(campoOrden,tipoOrden,where,cantidad,offset){
    return ipcRenderer.sendSync("getAll-alumno",campoOrden,tipoOrden,where,cantidad,offset)
}
