var sqlite3 = require('sqlite3');
var TABLA = "alumno";
function conectar(nombrBd, callbackError) {
    if (callbackError === void 0) { callbackError = undefined; }
    var db = new sqlite3.Database(nombrBd, sqlite3.OPEN_READWRITE, function (err) {
        if (err) {
            console.error("error: " + err.message);
            if (callbackError)
                callbackError();
        }
        console.log('Connected to SQLite the database.');
    });
    return db;
}
function count(nombreBd, where, callback) {
    var sql = "SELECT count(*) as cantidad FROM ".concat(TABLA, " ");
    if (where != "")
        sql += " where ".concat(where, " ");
    var conn = conectar(nombreBd);
    if (!conn)
        return null;
    else {
        conn.all(sql, [], callback);
        conn.close();
    }
}
function get(nombreBd, id, callback) {
    var sql = "SELECT * FROM ".concat(TABLA, " where id = ?");
    console.log(sql);
    var conn = conectar(nombreBd);
    if (!conn)
        return null;
    else {
        conn.get(sql, [id], callback);
        conn.close();
    }
}
function getAll(nombreBd, callback, campoOrden, tipoOrden, where, limit, offset) {
    if (campoOrden === void 0) { campoOrden = "nombre"; }
    if (tipoOrden === void 0) { tipoOrden = "asc"; }
    if (where === void 0) { where = ""; }
    if (limit === void 0) { limit = 0; }
    if (offset === void 0) { offset = 0; }
    var sql = "SELECT * FROM ".concat(TABLA, " ");
    if (where != "")
        sql += " where ".concat(where, " ");
    sql += " order by ".concat(campoOrden, " ").concat(tipoOrden);
    if (limit != 0)
        sql += " limit ".concat(limit, " offset ").concat(offset);
    console.log(sql);
    var conn = conectar(nombreBd);
    if (!conn)
        return null;
    else {
        conn.all(sql, [], callback);
        conn.close();
    }
}
function insert(nombreBd, datos, callback) {
    var sql = "insert into alumno (cedula,nombre,correo) values (?,?,?)";
    var conn = conectar(nombreBd);
    if (!conn)
        return null;
    else {
        conn.run(sql, [datos.cedula, datos.nombre, datos.correo], callback);
        conn.close();
    }
}
function update(nombreBd, datos, callback) {
    var sql = "update alumno set cedula=?,nombre=?,correo=? where id=?";
    var conn = conectar(nombreBd);
    if (!conn)
        return null;
    else {
        conn.run(sql, [datos.cedula, datos.nombre, datos.correo, datos.id], callback);
        conn.close();
    }
}
function remove(nombreBd, id, callback) {
    var sql;
    if (Array.isArray(id))
        sql = "delete from alumno where id in (".concat(id, ")");
    else
        sql = "delete from alumno where id=".concat(id);
    var conn = conectar(nombreBd);
    if (!conn)
        return null;
    else {
        conn.run(sql, [], callback);
        conn.close();
    }
}
module.exports = { conectar: conectar, getAll: getAll, count: count, insert: insert, remove: remove, get: get, update: update };
//# sourceMappingURL=db_sqlite.js.map