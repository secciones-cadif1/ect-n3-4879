var _a = require("electron"), app = _a.app, BrowserWindow = _a.BrowserWindow, dialog = _a.dialog, ipcMain = _a.ipcMain;
var appDataSource = require("./data-source").appDataSource;
var Alumno = require("./entidades/Alumno").Alumno;
var Ciudad = require("./entidades/Ciudad").Ciudad;
var Like = require("typeorm").Like;
appDataSource.initialize()
    .then(function () {
    console.log("datasource inicializado exitosamente");
})
    .catch(function (error) { return console.log(error); });
ipcMain.on("guardar-alumno", function (event, datos) {
    // para nuevos registros y para registros existentes
    // create y update. Si el objeto existe, lo actualiza
    // si no existe, lo inserta en la tabla
    appDataSource.getRepository(Alumno).save(datos)
        .then(function () {
        event.returnValue = 1;
    })
        .catch(function () {
        event.returnValue = -1;
    });
});
ipcMain.on("getAll-alumno", function (event, campoOrden, tipoOrden, campoWhere, valorWhere, limit, offset) {
    var _a, _b;
    if (campoOrden === void 0) { campoOrden = "nombre"; }
    if (tipoOrden === void 0) { tipoOrden = "asc"; }
    if (campoWhere === void 0) { campoWhere = ""; }
    if (valorWhere === void 0) { valorWhere = ""; }
    if (limit === void 0) { limit = 0; }
    if (offset === void 0) { offset = 0; }
    var opciones = {
        take: limit,
        skip: offset,
        order: (_a = {},
            _a[campoOrden] = tipoOrden,
            _a),
        where: (_b = {},
            _b[campoWhere] = Like(valorWhere),
            _b)
    };
    appDataSource.getRepository(Alumno).find(opciones)
        .then(function (registros) {
        event.returnValue = registros;
    })
        .catch(function (error) {
        console.log("error: " + error);
        event.returnValue = [];
    });
});
ipcMain.on("get-ciudades", function (event) {
    appDataSource.getRepository(Ciudad).find({
        order: {
            nombre: "ASC"
        }
    })
        .then(function (ciudades) {
        event.returnValue = ciudades;
    })
        .catch(function (error) {
        console.log("error: " + error);
        event.returnValue = null;
    });
});
ipcMain.on("get-ciudad", function (event, idValue) {
    appDataSource.getRepository(Ciudad).findOneBy({
        id: idValue,
    })
        .then(function (ciudad) {
        console.log(ciudad);
        event.returnValue = ciudad;
    })
        .catch(function (error) {
        console.log(error);
        event.returnValue = -1;
    });
});
ipcMain.on("get-alumno", function (event, idValor) {
    appDataSource.getRepository(Alumno).findOneBy({
        id: idValor
    })
        .then(function (alumno) {
        console.log(alumno);
        event.returnValue = alumno;
    })
        .catch(function (error) {
        console.log("error: " + error);
        event.returnValue = null;
    });
});
ipcMain.on("remove-alumno", function (event, id) {
    appDataSource.getRepository(Alumno).delete(id)
        .then(function () {
        event.returnValue = 1;
    })
        .catch(function () {
        event.returnValue = -1;
    });
});
ipcMain.on("count-alumno", function (event, campoWhere, valorWhere) {
    appDataSource.getRepository(Alumno)
        .createQueryBuilder("cliente")
        .where("".concat(campoWhere, " like '").concat(valorWhere, "'"))
        .getCount()
        .then(function (valor) {
        event.returnValue = valor;
    })
        .catch(function (error) {
        console.log(error);
        event.returnValue = -1;
    });
});
console.log(__dirname);
app.on("ready", function () {
    var ventana = new BrowserWindow({
        visible: false,
        webPreferences: {
            preload: __dirname + "\\preload.js",
            nodeIntegration: false,
            contextIsolation: true
        }
    });
    //  ventana.setMenu(null)
    ventana.loadFile("../clase4/alumnos.html");
    ventana.maximize();
    ventana.webContents.openDevTools();
    ventana.show();
});
//# sourceMappingURL=main.js.map