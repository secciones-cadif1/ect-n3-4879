"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.appDataSource = void 0;
var typeorm_1 = require("typeorm");
var Alumno_1 = require("./entidades/Alumno");
var Ciudad_1 = require("./entidades/Ciudad");
exports.appDataSource = new typeorm_1.DataSource({
    type: "sqlite",
    database: "datos.db",
    logging: true,
    entities: [Alumno_1.Alumno, Ciudad_1.Ciudad],
});
//# sourceMappingURL=data-source.js.map