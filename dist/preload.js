var _a = require("electron"), ipcRenderer = _a.ipcRenderer, contextBridge = _a.contextBridge;
//import {Alumno} from "./entidades/Alumno"
contextBridge.exposeInMainWorld("api", {
    "saveAlumno": saveAlumno,
    "getAlumno": getAlumno,
    "getCountAlumnos": getCountAlumnos,
    "getAllAlumnos": getAllAlumnos,
    "removeAlumno": removeAlumno,
    "getCiudades": getCiudades
});
function getCiudad(id) {
    var ciudad = ipcRenderer.sendSync("get-ciudad", id);
    return ciudad;
}
function getCiudades() {
    var ciudades = ipcRenderer.sendSync('get-ciudades');
    return ciudades;
}
function getAlumno(id) {
    var result = ipcRenderer.sendSync("get-alumno", id);
    return result;
}
function removeAlumno(datos) {
    var result = ipcRenderer.sendSync("remove-alumno", datos);
    return result;
}
function saveAlumno(form) {
    var alumno = new Alumno();
    if (form.idAlumno.value != 0)
        alumno.id = parseInt(form.idAlumno.value);
    alumno.cedula = form.cedula.value;
    alumno.correo = form.correo.value;
    alumno.nombre = form.nombre.value;
    alumno.ciudad = getCiudad(form.idCiudad.value);
    var result = ipcRenderer.sendSync("guardar-alumno", alumno);
    return result;
}
function getCountAlumnos(campoWhere, valorWhere) {
    return ipcRenderer.sendSync("count-alumno", campoWhere, valorWhere);
}
function getAllAlumnos(campoOrden, tipoOrden, campoWhere, valorWhere, cantidad, offset) {
    return ipcRenderer.sendSync("getAll-alumno", campoOrden, tipoOrden, campoWhere, valorWhere, cantidad, offset);
}
//# sourceMappingURL=preload.js.map